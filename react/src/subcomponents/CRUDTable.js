import React, { Component } from 'react';

import {
    Table,
    TableBody,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';

class CRUDTable extends Component {

    state = {
        deselectOnClickaway: false,
        selectedRow: [],
    };

    // This Material UI handler returns either null or an array of the selected rows.
    // There is some odd behaviourwith deselectOnClickaway at the table level.
    handleRowSelection = (selectedRows) => {
        this.setState({
            selectedRow: selectedRows
        });
        this.props.updateSelectedTicket(selectedRows);
    }

    isSelected(index){
        return this.state.selectedRow.includes(index)
    }

    render() {

            const tickets = this.props.tickets;

            const styles = {
                smallColumn: {
                    width: '5%'
                },
                mediumColumn: {
                    width: '10%'
                },
                largeColumn: {
                    width: '14%'
                }
            };

            return (
                <Table
                    onRowSelection={this.handleRowSelection}
                >
                    <TableBody
                        deselectOnClickaway={this.state.deselectOnClickaway}
                    >
                        {tickets.map( (ticket, i) => {
                            return (
                                <TableRow key={i} selected={this.isSelected(i)}>
                                    <TableRowColumn style={styles.smallColumn}>{ticket.id}</TableRowColumn>
                                    <TableRowColumn style={styles.mediumColumn}>{ticket.created}</TableRowColumn>
                                    <TableRowColumn style={styles.largeColumn}>{ticket.user}</TableRowColumn>
                                    <TableRowColumn style={styles.smallColumn}>{ticket.escalation}</TableRowColumn>
                                    <TableRowColumn style={styles.mediumColumn}>{ticket.status}</TableRowColumn>
                                    <TableRowColumn style={styles.mediumColumn}>{ticket.priority}</TableRowColumn>
                                    <TableRowColumn style={styles.mediumColumn}>{ticket.os}</TableRowColumn>
                                    <TableRowColumn style={styles.mediumColumn}>{ticket.software}</TableRowColumn>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            );
        }
    }

export default CRUDTable