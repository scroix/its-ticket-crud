import React, { Component } from 'react';
import EscalateTicket from './actions/tech/EscalateTicket.js'
import Responses from './actions/tech/Responses.js'
import AssignTech from './actions/helpdesk/AssignTech.js'
import ViewTicket from './actions/helpdesk/UpdateTicket.js'

import {
    Table,
    TableHeader,
    TableHeaderColumn,
    TableRow,
} from 'material-ui/Table';

import Escalation from 'material-ui/svg-icons/action/trending-up';
import RaisedButton from 'material-ui/RaisedButton';

class Taskbar extends Component {

    render() {

        const {
            handleDelete,
            handleAssignTech,
            pushUpdatedTicket,
            pushNewResponse,
            submitRevokeTech,
            showManager,
            techUsers,
            selectedTech,
            selectedTicket,
            ticketResponses
        } = {...this.props};

        const styles = {
            smallColumn: {
                width: '5%'
            },
            mediumColumn: {
                width: '10%'
            },
            largeColumn: {
                width: '14%'
            }
        };

        return (
            <Table>
                {showManager === true ?
                    <TableHeader>
                        {this.props.userType === 'helpdesk' ?
                            <TableRow>
                                <TableHeaderColumn colSpan={4}>
                                    <AssignTech
                                        techUsers={techUsers}
                                        handleAssignTech={handleAssignTech}
                                        selectedTech={selectedTech}
                                    />
                                </TableHeaderColumn>,
                                <TableHeaderColumn>
                                    <ViewTicket
                                        pushUpdatedTicket={pushUpdatedTicket}
                                        selectedTicket={selectedTicket}
                                    />
                                </TableHeaderColumn>,
                                <TableHeaderColumn>
                                    <RaisedButton
                                        label="Delete"
                                        secondary={true}
                                        onClick={handleDelete}
                                    />
                                </TableHeaderColumn>
                            </TableRow>
                            :
                            <TableRow>
                                <TableHeaderColumn colSpan={3}>
                                </TableHeaderColumn>,
                                <TableHeaderColumn>
                                    <Responses
                                        ticketResponses={ticketResponses}
                                        selectedTicket={selectedTicket}
                                        pushUpdatedTicket={pushUpdatedTicket}
                                        pushNewResponse={pushNewResponse}
                                    />
                                </TableHeaderColumn>,
                                <TableHeaderColumn>
                                    <EscalateTicket
                                        pushUpdatedTicket={pushUpdatedTicket}
                                        selectedTicket={selectedTicket}
                                        submitRevokeTech={submitRevokeTech}
                                    />
                                </TableHeaderColumn>,
                                <TableHeaderColumn>
                                    <RaisedButton
                                        label="Delete"
                                        secondary={true}
                                        onClick={handleDelete}
                                    />
                                </TableHeaderColumn>
                            </TableRow>
                        }
                    </TableHeader>
                    :
                    <TableHeader>
                        <TableRow>
                            <TableHeaderColumn style={styles.smallColumn}>ID</TableHeaderColumn>
                            <TableHeaderColumn style={styles.mediumColumn}>Created</TableHeaderColumn>
                            <TableHeaderColumn style={styles.largeColumn}>User</TableHeaderColumn>
                            <TableHeaderColumn style={styles.smallColumn}><Escalation color={'#9e9eba'}/></TableHeaderColumn>
                            <TableHeaderColumn style={styles.mediumColumn}>Status</TableHeaderColumn>
                            <TableHeaderColumn style={styles.mediumColumn}>Priority</TableHeaderColumn>
                            <TableHeaderColumn style={styles.mediumColumn}>Platform</TableHeaderColumn>
                            <TableHeaderColumn style={styles.mediumColumn}>Software</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                }
            </Table>
        );
    }
}

export default Taskbar