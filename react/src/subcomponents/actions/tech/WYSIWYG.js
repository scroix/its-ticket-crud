import React from 'react';
import {Editor, EditorState, RichUtils, convertToRaw} from 'draft-js';
import draftToHtml from 'draftjs-to-html';


export default class BasicHtmlEditorExample extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            editorState: EditorState.createEmpty(),
            rawHTML: ''
        };
    }

    onChange = (editorState) => {
        this.setState({editorState}, );
        this.toHTML(editorState);
    };

    // Convert the editor object's value into raw HTML for our parent to process.
    toHTML(editorState) {
        const rawContentState = convertToRaw(editorState.getCurrentContent());
        const markup = draftToHtml(rawContentState);
        this.setState({rawHTML: markup});
        this.props.updateHTML(markup);
    };

    _onBoldClick() {
        this.onChange(RichUtils.toggleInlineStyle(
            this.state.editorState,
            'BOLD'
    ));
    }

    _onItalicClick() {
        this.onChange(RichUtils.toggleInlineStyle(
            this.state.editorState,
            'ITALIC'
        ));
    }

    _onUnderlineClick() {
        this.onChange(RichUtils.toggleInlineStyle(
            this.state.editorState,
            'UNDERLINE'
        ));
    }

    _onCodeClick() {
        this.onChange(RichUtils.toggleInlineStyle(
            this.state.editorState,
            'CODE'
        ));
    }

    render () {

        const style = {
            textColour: {
                color: '#2169be'
            },
            containerSize: {
                margin: '5px 0px 10px 0px',
                borderBottomStyle: 'solid',
                borderBottomColor: '#D3D3D3',
                borderBottomWidth: '1px'
            }
        };

        return (
            <div id="content">
                <small style={style.textColour}>Enter response...</small>
                <div className='editor' style={style.containerSize}>
                    <Editor
                        editorState={this.state.editorState}
                        onChange={this.onChange}
                    />
                </div>
                <button onClick={this._onBoldClick.bind(this)}>Bold</button>
                <button onClick={this._onItalicClick.bind(this)}>Italic</button>
                <button onClick={this._onUnderlineClick.bind(this)}>Underline</button>
                <button onClick={this._onCodeClick.bind(this)}>Code</button>
            </div>

        )
    }

}