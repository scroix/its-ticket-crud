import React from 'react';
import WYSIWYG from './WYSIWYG.js';

import {
    Dialog,
    List,
    ListItem,
    Divider,
    TextField,
    SelectField,
    MenuItem,
    FlatButton,
    RaisedButton
} from 'material-ui'

import Comment from 'material-ui/svg-icons/communication/comment';
import {darkBlack} from 'material-ui/styles/colors';


export default class Responses extends React.Component {
    state = {
        open: false,
        responses: [],
        response: '',
        status: this.props.selectedTicket['status'],
        disabled: true,
    };

    componentWillReceiveProps() {
        if (typeof this.props.selectedTicket === "undefined") {
            return
        }
        this.setState({
            responses: this.props.ticketResponses,
            status: this.props.selectedTicket['status']
        })
    }

    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleSubmit = () => {
        this.setState({open: false}, this.pushSubmission);
    };

    htmlToResponse = (rawHTML) => {
        this.setState({response: rawHTML}, this.handleSubmitButton)
    }

    // Require a non-pending state and existing response for a valid update
    handleSubmitButton() {
        if (this.state.status !== 'Pending' && this.state.response.length > 0) {
            this.setState({ disabled: false})
        }
        else {
            this.setState({ disabled: true })
        }
    }

    // Store status select field value as a state for evaluation
    // Callback to update submission button status
    handleStatusChange = (event, index, value) => {
        this.setState({status: value}, this.handleSubmitButton);
    };

    // There are two (potential) API calls.
    // (1) Tickets table to update the status ("Pending" -> "[Status]")
    // (2) Response table with the addition of a new response
    pushSubmission() {
        // Case 1
        if (this.state.status !== this.props.selectedTicket['status']){
            this.props.pushUpdatedTicket({'status': this.state.status})
        }
        // Case 2
        if (this.state.response.length > 0) {
            this.props.pushNewResponse({
                response: this.state.response,
                ticket_id: this.props.selectedTicket['id']
            })
        }

    }

    render() {

            const styles = {
            errorStyle: {
                color: '#e42a20',
            },
            underlineStyle: {
                borderColor: '#1565c0',
            },
            floatingLabelStyle: {
                color: '#1565c0',
            },
            floatingLabelFocusStyle: {
                color: '#e42a20',
            },
            commentBox: {
                padding: '0px 0px 0px 72px'
            },
        };

        const responses = this.props.ticketResponses;
        const ticket = this.props.selectedTicket;

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.handleClose}
            />,
            <FlatButton
                label="Submit"
                primary={true}
                disabled={this.state.disabled}
                onClick={this.handleSubmit}
            />,
        ];

            return (

            <div>
                <RaisedButton label="Comments" default={true} onClick={this.handleOpen} />
                {typeof ticket !== "undefined" ?
                <Dialog
                    title={ticket['user']}
                    actions={actions}
                    modal={true}
                    open={this.state.open}
                >
                    <subheading>{ticket['comment']}</subheading><br />
                    {responses.length > 0 ?
                            <List>
                            {responses.map( (response, i) => {
                                return (
                                    <div key={"outer-div" + i}>
                                    <ListItem
                                        key={i}
                                        disabled={true}
                                        leftAvatar={<Comment/>}
                                        primaryText={<span style={{color: darkBlack}}>{response.created_at}</span>}
                                        secondaryText={ <div dangerouslySetInnerHTML={{ __html: response.response.replace("<p>", "").replace("</p>", "") }}/> }
                                        secondaryTextLines={1}
                                    />
                                    <Divider key={"divider" + i} inset={true} />
                                    </div>
                                )
                            })}
                            </List> : null }
                            <div style={styles.commentBox}>
                            <br />
                                <WYSIWYG updateHTML={this.htmlToResponse.bind(this)}/>
                            <br /><SelectField
                                floatingLabelText="Status"
                                floatingLabelStyle={styles.floatingLabelStyle}
                                value={this.state.status}
                                onChange={this.handleStatusChange}
                            >
                                <MenuItem value={"Pending"} primaryText="Pending" />
                                <MenuItem value={"In Progress"} primaryText="In Progress" />
                                <MenuItem value={"Unresolved"} primaryText="Unresolved" />
                                <MenuItem value={"Resolved"} primaryText="Resolved" />
                            </SelectField>
                            </div>
                </Dialog>
                    : null }
            </div>
        );
    }
}