import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';


export default class Escalate extends React.Component {

    handleEscalation = () => {
        // Only valid tickets
        if (typeof this.props.selectedTicket === "undefined") {
            return;
        }

        // Increment to a maximum escalation level
        let escalation = this.props.selectedTicket['escalation'];
        if (escalation < 3) { escalation++; }

        // Update!
        this.props.pushUpdatedTicket({escalation: escalation});
        this.props.submitRevokeTech();
    }

    render() {

        return (

            <div >
                <RaisedButton label="Escalate" primary={true} onClick={this.handleEscalation} />
            </div>
        );
    }
}