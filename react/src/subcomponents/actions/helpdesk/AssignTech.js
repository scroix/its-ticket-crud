import React, {Component} from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const styles = {
    overflow: "hidden"
};

/**
 * `SelectField` is implemented as a controlled component,
 * with the current selection set through the `value` property.
 * The `SelectField` can be disabled with the `disabled` property.
 */
export default class Assignment extends Component {
    state = {
        value: null,
    };

    handleChange = (event, index, value) => {
        this.setState({value});
        this.props.handleAssignTech({value})
    }

    render() {
        return (
            <div>
                <SelectField
                    hintText="Assign a tech"
                    style={styles}
                    value={this.props.selectedTech}
                    onChange={this.handleChange}
                >
                    {this.props.techUsers.map((user, i ) => (
                        <MenuItem key={i} value={user.id} primaryText={user.name} />
                    ))}
                </SelectField>
            </div>
        )
    }
}

