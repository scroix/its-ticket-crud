import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';


import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';


/**
 * A modal dialog can only be closed by selecting one of the actions.
 */
export default class HelpdeskEditor extends React.Component {
    state = {
        open: false,
        status: this.props.selectedTicket['status'],
        priority: this.props.selectedTicket['priority'],
        escalation: this.props.selectedTicket['escalation']
    };

    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleSubmit = () => {
        this.setState({open: false});
        // Create a duplicate ticket from our updated state,
        // and remove the unnecessary 'open' key.
        let newTicket = this.state;
        delete newTicket['open'];
        // Send this to our grand-parent.
        this.props.pushUpdatedTicket(newTicket);
    };

    handleStatusChange = (event, index, value) => { this.setState({status: value}); };
    handlePriorityChange = (event, index, value) => { this.setState({priority: value}); };
    handleEscalationChange = (event, index, value) => { this.setState({escalation: value}); };

    render() {

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.handleClose}
            />,
            <FlatButton
                label="Submit"
                primary={true}
                disabled={false}
                onClick={this.handleSubmit}
            />,
        ];

        const ticket = this.props.selectedTicket;

        const styles = {
            dialogWidth: {
                width: '50%',
                maxWidth: '400px',
            }
        };

        return (

            <div >
                <RaisedButton label="Update" primary={true} onClick={this.handleOpen} />
                {typeof ticket !== "undefined" ?
                <Dialog
                    title={ticket['user']}
                    actions={actions}
                    modal={true}
                    open={this.state.open}
                    contentStyle={styles.dialogWidth}
                >
                    <i>{ticket['comment']}</i><br />
                    <TextField
                    disabled={true}
                    defaultValue={ticket['id']}
                    floatingLabelText="TicketID"
                /><br />
                    <SelectField
                        floatingLabelText="Status"
                        value={this.state.status}
                        onChange={this.handleStatusChange}
                    >
                        <MenuItem value={"Pending"} primaryText="Pending" />
                        <MenuItem value={"In Progress"} primaryText="In Progress" />
                        <MenuItem value={"Unresolved"} primaryText="Unresolved" />
                        <MenuItem value={"Resolved"} primaryText="Resolved" />
                    </SelectField><br />
                    <SelectField
                        floatingLabelText="Priority"
                        value={this.state.priority}
                        onChange={this.handlePriorityChange}
                    >
                        <MenuItem value={"Low"} primaryText="Low" />
                        <MenuItem value={"Moderate"} primaryText="Moderate" />
                        <MenuItem value={"High"} primaryText="High" />
                    </SelectField><br />
                    <SelectField
                        floatingLabelText="Escalation Level"
                        value={this.state.escalation}
                        onChange={this.handleEscalationChange}
                    >
                        <MenuItem value={"1"} primaryText="1" />
                        <MenuItem value={"2"} primaryText="2" />
                        <MenuItem value={"3"} primaryText="3" />
                    </SelectField>
                </Dialog>
                    : null }
            </div>
        );
    }
}