import React, { Component } from 'react';
import Paper from 'material-ui/Paper';

import logo from '../_static/RMIT_University_Logo.svg'
import RaisedButton from 'material-ui/RaisedButton';

class Login extends Component {

    handleClick = (userType) => {
        this.props.handleClick(userType)
    }

    render() {

        const buttonStyle = {
                margin: 12,
            };

        const style = {
                height: 350,
                width: 350,
                margin: 20,
                textAlign: 'center',
                display: 'inline-block',
                verticalAlign: 'middle',
                paddingTop: 50,
            };


        return (
        <div className="text-center" style={{display: 'flex', justifyContent: 'center', paddingTop: 125}}>
            <Paper style={style} zDepth={2}>
                <div>
                    <img src={logo} className="App-logo" alt="logo" style={{width: 225}}/>
                    <h1>Sign in to continue</h1>
                    <p>
                        Please select your account type:
                    </p>
                    <div className="text-center">
                        <RaisedButton label="Helpdesk" primary={true} style={buttonStyle}
                                      onClick={() => this.handleClick('helpdesk')}/>
                        <RaisedButton label="Tech" secondary ={true}style={buttonStyle} onClick={() => this.handleClick('tech')}/>
                    </div>
                </div>
            </Paper>
        </div>
    );

}

}

export default Login;