import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
import App from './components/App';
import firebase from 'firebase';

/* Firebase */
const config = {
    apiKey: "YOUR KEY",
    authDomain: "YOUR AUTH DOMAIN",
    databaseURL: "YOUR DATABASE URL",
    projectId: "YOUR PROJECT ID",
    storageBucket: "",
    messagingSenderId: "YOUR SENDER ID"
};
firebase.initializeApp(config);


render(<BrowserRouter>
         <App />
       </BrowserRouter>, document.getElementById('root'));
registerServiceWorker();
