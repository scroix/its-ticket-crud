import React, { Component } from 'react';
import autoBind from 'react-autobind';
import firebase from 'firebase';
import AlertContainer from 'react-alert'
import CRUDTable from '../subcomponents/CRUDTable.js'
import Taskbar from '../subcomponents/Taskbar.js'

const apiURL = 'http://localhost/wda/a2/public/api';

class Dashboard extends Component {

    constructor(props) {

        super(props);

        autoBind(this);

        this.state = {
            tickets: [],
            selectedTicket: null,
            techUsers: [],
            selectedTech: null,
            showManager: false,
            ticketResponses: []
        };

    }

    componentDidMount(){
        this.getTickets();
        this.getAssignments();
    }

    /*
     A series of API calls between React/Laravel, and React/Firebase
            getTickets      : Retrieves and displays tickets based on the type of user.
            getAssignments  : Provides a list of all the registered tech users.
            getResponses    : Pulls just the comments/responses on any selected ticket.
     */

    getTickets() {
        fetch(apiURL+"/tickets")
            .then((response) => response.json())
            .then((responseJson) => {
                console.log("Downloaded tickets.");
                const pendingTickets = [];
                for(const ele in responseJson) {
                    // divide the tickets up into assigned and non-assigned like the lab9 code.
                    firebase.database().ref('ticket/'+responseJson[ele].id).on('value', (snapshot) => {
                        if( (snapshot.val() === null) && (this.props.type === 'helpdesk') ) {
                            pendingTickets.push(responseJson[ele]);
                            /* Force the view to re-render (async problem) */
                            this.forceUpdate();
                        }
                        else if ( (snapshot.val() !== null) && (snapshot.val().user_id === this.props.user.uid) && (this.props.type === 'tech') ) {
                            pendingTickets.push(responseJson[ele]);

                            /* Force the view to re-render (async problem) */
                            this.forceUpdate();
                        }
                    })
                }
                return pendingTickets;
            })
            .then((tickets) => {
                this.setState({
                    tickets: tickets
                });
            })
            .catch((error) => {
                console.error(error);
            });
    }

    getAssignments() {
        /* Creates a firebase listener which will automatically
         update the list of tech users every time a new tech
         registers into the system
         */
        const users = firebase.database().ref('user/');
        users.on('value', (snapshot) => {
            const tempTech = [];
            for(const ele in snapshot.val()) {
                if(snapshot.val()[ele].type === 'tech') {
                    tempTech.push(snapshot.val()[ele]);
                }
            }
            this.setState({
                techUsers: tempTech
            });
        })
    }

    getResponses(selected) {
        if (selected == null) {
            return;
        }
        // Map from our displayed array, to the ticket array.
        let ticket = JSON.stringify(this.state.tickets[selected]['id']);
        // Fetch just the responses for that ticket.
        fetch(apiURL+"/responses/"+ticket)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log("Downloaded responses.");
                this.setState({
                    ticketResponses: responseJson
                });
            })
            .catch((error) => {
                console.error(error);
            });
    }

    /*
     A series of API calls between React/Laravel, and React/Firebase
     submitUpdate       : A PUT request to Laravel provided an updated ticket object.
     submitResponse     : A POST request to Laravel creating a new response object.
     submitDelete       : A DELETE request to Laravel to destroy a ticket object.
     submitAssignTech   : A reference call to Firebase to create a new ticket/tech association.
     submitRevokeTech : A reference call to Firebase to destroy an existing ticket/tech association.
     */

    submitUpdate(content) {

        if (this.state.selectedTicket === null) {
            return;
        }

        const id = JSON.stringify(this.state.tickets[this.state.selectedTicket]['id']);

        const bodyString = JSON.stringify(content)

        fetch(apiURL+"/tickets/"+id, {
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'PUT',
            body: bodyString
        }).then(
            (response) => {
                this.getTickets();
            }
        );

        this.msg.success('Ticket updated')
    }

    submitResponse(response) {

        const bodyString = JSON.stringify(response)

        fetch(apiURL+"/responses/", {
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: bodyString
        }).then(
            (response) => {
                this.getTickets();
            }
        );

        this.msg.success('Response created')
    }

    submitDelete() {
        let id = JSON.stringify(this.state.tickets[this.state.selectedTicket]['id']);

        fetch(apiURL+"/tickets/"+id, {
            method: 'DELETE'
        }).then(
            (response) => {
                this.getTickets();
            }
        );
    }

    // A handler for the delete which limits the tech users delete capacity before
    // allowing a submitDelete to execute.
    handleDelete() {
        if (!this.actionIsSafe()) {
            return;
        }
        // A helpdesk user can delete without hesitation,
        // but a tech user must first meet ticket requirements.
        if (this.props.type === 'tech') {
            if ( (this.state.tickets[this.state.selectedTicket].status !== 'Unresolved') &&
                (this.state.tickets[this.state.selectedTicket].status !== 'Resolved')) {
                this.msg.error('Unable to delete active tickets');
                return;
            }
        }

        this.submitDelete(); // Update our database with the API
        this.setState({
            ticketManager: false
        });

        this.msg.info('Ticket deleted') // A cute notification
    }

    submitAssignTech(tech) {
        if(tech === null || typeof this.state.selectedTicket === "undefined") {
            this.setState({
                showManager: false
            });
            return;
        }

        /* Add assigned ticket+tech into database*/
        const data = {};
        data['ticket/' + this.state.tickets[this.state.selectedTicket].id] = {
            ticket_id: this.state.tickets[this.state.selectedTicket].id,
            user_id: tech // stored Tech ID
        };
        firebase.database().ref().update(data)
            .then(
                (response) => {
                    this.getTickets();
                }
            );

        this.msg.success('Ticket assigned')
    }

    // A handler for the assignTech which ensures a valid selection before
    // allowing a submitAssignTech to execute.
    handleAssignTech(tech) {
        if (!this.actionIsSafe()) {
            return;
        }

        if (tech !== -1) {
            this.setState({
                selectedTech: tech['value'],
            });
        }
        this.submitAssignTech(tech['value']);
    }

    submitRevokeTech() {
        const ticket = 'ticket/' + this.state.tickets[this.state.selectedTicket].id;
        firebase.database().ref().child(ticket).remove()
            .then(
                (response) => {
                    this.getTickets();
                }
            );
    }

    /*
     A series of functions to support state handling.
     updateSelectedTicket    : Passed as props to children, exposes a particular ticket from our tickets array.
     actionIsSafe            : Reduces some "undefined" issues that come from handling an asynchronous interface.
     */

    updateSelectedTicket(ticket) {
        this.setState({
            selectedTicket: ticket[0],
            selectedTech: null
        }, this.getResponses(ticket[0]));
        if (ticket.length > 0) {
            this.setState({
                showManager: true
            })
        }
        else {
            this.setState({
                showManager: false
            })
        }
    }

    actionIsSafe() {
        if (typeof this.state.tickets[this.state.selectedTicket] === "undefined") {
            this.setState({
                showManager: false
            });
            return false;
        }
        return true;
    }

    alertOptions = {
        offset: 14,
        position: 'bottom right',
        theme: 'light',
        time: 5000,
        transition: 'fade'
    }

    render() {

        const props = {
            handleDelete: this.handleDelete,
            handleAssignTech: this.handleAssignTech,
            pushUpdatedTicket: this.submitUpdate,
            pushNewResponse: this.submitResponse,
            submitRevokeTech: this.submitRevokeTech,
            showManager: this.state.showManager,
            techUsers: this.state.techUsers,
            selectedTech: this.state.selectedTech,
            selectedTicket: this.state.tickets[this.state.selectedTicket],
            ticketResponses: this.state.ticketResponses
        }

        return (
            <div>
                <Taskbar
                    {...props}
                    userType={this.props.type}
                />
                <CRUDTable
                    tickets={this.state.tickets}
                    updateSelectedTicket={this.updateSelectedTicket}/>
                <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
            </div>

        );
    }
}

export default Dashboard;
