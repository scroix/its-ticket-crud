import React, { Component } from 'react';
import logo from '../_static/rmit-logo.png'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

// Material UI Components
import {
    MuiThemeProvider,
    AppBar,
    IconButton,
    FlatButton,
    IconMenu,
    MenuItem,
} from 'material-ui'

// Theme
import getMuiTheme      from 'material-ui/styles/getMuiTheme';


// The muiTheme we apply to MuiThemeProvider
const muiTheme = getMuiTheme({
    palette: {
        "primary1Color": "#1565c0",
        "primary2Color": "#1e88e5",
        "primary3Color": "#42a5f5",
        "accent1Color": "#e42a20"
    },
})

class Login extends Component {
    static muiName = 'FlatButton';

    render() {
        return (
            <FlatButton {...this.props} label="Login" />
        );
    }
}

const Logged = (props) => (
    <IconMenu
        {...props}
        iconButtonElement={
            <IconButton><MoreVertIcon /></IconButton>
        }
        targetOrigin={{horizontal: 'right', vertical: 'top'}}
        anchorOrigin={{horizontal: 'right', vertical: 'top'}}
    >
        <MenuItem value="1" primaryText="Refresh" />
        <MenuItem value="2" primaryText="Help" />
        <MenuItem value="3" primaryText="Sign out" />
    </IconMenu>
);

Logged.muiName = 'IconMenu';

class Header extends Component {

    constructor (props) {
        super(props);

        this.state = {
            valueSingle: '0'
        }
    }

    handleChangeSingle = (event, value) => {
        this.setState({
            valueSingle: value,
        });
        if (value === '3') {
            this.props.handleSignout();
        }
        this.setState({
            valueSingle: 0,
        });
    };

    render () {

        return (
            <MuiThemeProvider muiTheme={muiTheme}>
            <AppBar
                title="Helpdesk"
                showMenuIconButton={true}
                iconElementLeft={<img src={logo} alt="RMIT Logo"/>}
                iconElementRight={this.props.user !== null ? <Logged onChange={this.handleChangeSingle} value={this.state.valueSingle}/> : <Login />}
            />
            </MuiThemeProvider>
        );
    }
}

export default Header;