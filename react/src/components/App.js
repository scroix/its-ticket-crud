import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import firebase from 'firebase';
import autoBind from 'react-autobind';

import Header from './Header.js';
import Dashboard from './Dashboard.js';
import Login from '../subcomponents/LoginSplash.js'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import '../index.css';

// Theme
import * as Colours from 'material-ui/styles/colors';
import getMuiTheme  from 'material-ui/styles/getMuiTheme';

// The muiTheme we apply to MuiThemeProvider
const muiTheme = getMuiTheme({
    palette: {
        primary1Color: Colours.blue800,
        accent1Color: Colours.redA400,
    },
});

/*  This component is almost identical to the Lab 9 code
 *  which was developed by Nicholas Zuccarelli.
 *  Thanks, Nick! It's a beautiful thing.
 */

class App extends Component {

    constructor(props) {
        super(props);

        autoBind(this);

        this.state = {
            type: null,
            user: null
        }
    }

    componentWillMount () {
        firebase.auth().onAuthStateChanged(this.handleCredentials);
    }

    componentWillUnmount() {
        if(this.state.user !== null) {
            localStorage.setItem('type', this.state.type);
        }
    }

    handleClick = (type) => {
        const provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider)
            .then((success) => { this.handleCredentials(success.user) })
            .then(() => { this.handleLogin(type) });
    }

    handleCredentials = (params) => {
        console.log(params);
        this.setState({
            user: params,
            type: localStorage.getItem('type')
        });
    }

    handleLogin = (type) => {
        localStorage.setItem('type', type);
        this.setState({
            type: type
        });

        /* MongoDB Schema - Insert User's Details */
        const user = {};
        user['user/' + this.state.user.uid] = {
            type: type,
            name: this.state.user.displayName,
            id: this.state.user.uid
        };
        firebase.database().ref().update(user)
    }

    handleSignout = () => {
        const vm = this;
        vm.setState({
            user: null,
            type: null
        });
        localStorage.setItem('type', null);
        firebase.auth().signOut();
    }

    render() {

        return (
            <MuiThemeProvider muiTheme={muiTheme}>
              <div>
                  <Header user={this.state.user} handleSignout={this.handleSignout} />
                  <div className="container">
                      <Route exact path="/" render={() => (
                          this.state.user === null ? ( <Login handleClick={this.handleClick}/> )
                              : ( <Redirect to="/dashboard" /> )
                      )} />
                      <Route exact path="/dashboard" render={() => (
                          this.state.user !== null ? ( <Dashboard user={this.state.user} type={this.state.type} /> )
                              : ( <Redirect to="/" /> )
                      )} />
                  </div>
              </div>
            </MuiThemeProvider>
        );
    }
}

export default App;