<?php

use App\Models\Patron;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return view('home', ['template' => 'user']);
});

// Shhhh!
Route::get('/easter', function() {
    return view('easter');
});

Route::resource('ticket', 'TicketController');

Route::resource('admin', 'ResponseController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
