<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('tickets', 'WaiterController@index');
Route::get('responses/{ticket}', 'WaiterController@show');
Route::post('responses', 'WaiterController@store');
Route::put('tickets/{ticket}', 'WaiterController@update');
Route::delete('tickets/{ticket}', 'WaiterController@delete');
