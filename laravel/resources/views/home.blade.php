@extends('layouts.master', ['template' => 'user'])
@section('title', 'Home')
    @section('content')
        <!-- Header -->
        <a name="about"></a>
        <div class="intro-header">
            <div class="container">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="intro-message">
                            <h1>Helpdesk</h1>
                            <h3>A WDA Platform</h3>
                            <hr class="intro-divider">
                            @if (Auth::check())
                                <ul class="list-inline intro-buttons">
                                    <li>
                                        <a href="{{ url('ticket/create') }}" class="btn btn-default btn-lg"> <span class="network-name"><span class="glyphicon glyphicon-circle-arrow-right" aria-hidden="true"></span> Submit</span></a>
                                    </li>
                                    <li>
                                        <a href="{{ url('ticket/') }}" class="btn btn-default btn-lg"><span class="network-name"><span class="glyphicon glyphicon-equalizer" aria-hidden="true"></span> Track</span></a>
                                    </li>
                                </ul>
                            @else
                                <ul class="list-inline intro-buttons">
                                    <li>
                                        <a href="{{ url('register') }}" class="btn btn-default btn-lg"> <span class="network-name"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span> Register</span></a>
                                    </li>
                                    <li>
                                        <a href="{{ url('login') }}" class="btn btn-default btn-lg"> <span class="network-name"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Login</span></a>
                                    </li>
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container -->

        </div>
        <!-- /.intro-header -->

        <!-- Page Content -->

        <a  name="services"></a>
        <div class="content-section-a">

            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-sm-6">
                        <hr class="section-heading-spacer">
                        <div class="clearfix"></div>
                        <h2 class="section-heading">What <i>actually</i> is it?</h2>
                        <p class="lead">The WDA (Web Development Applications) Helpdesk is an RMIT student initiative designed to replace the existing <a target="_blank" href="https://rmit.service-now.com/serviceandsupport/">ITS Service & Support</a> system. </p>
                        <p class="lead">A great deal of effort has gone into streamlining the ticketing system for users. To begin, simply select the <b>Submit</b> link above and fill out the attached form.</p>
                    </div>
                    <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                        <img class="img-responsive" src="img/ipad.png" alt="">
                    </div>
                </div>

            </div>
            <!-- /.container -->

        </div>
        <!-- /.content-section-a -->

        <div class="content-section-b">

            <div class="container">

                <div class="row">
                    <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                        <hr class="section-heading-spacer">
                        <div class="clearfix"></div>
                        <h2 class="section-heading">Some frequently asked questions</h2>
                        <ul class="list-group">
                            <li class="list-group-item"><a class="list-faq" data-toggle="modal" data-target="#faq-1">How do I log into my RMIT account?</a></li>
                            <li class="list-group-item"><a class="list-faq" data-toggle="modal" data-target="#faq-2">I can't login.</a></li>
                            <li class="list-group-item"><a class="list-faq" data-toggle="modal" data-target="#faq-3">How do I change or reset my password?</a></li>
                            <li class="list-group-item"><a class="list-faq" data-toggle="modal" data-target="#faq-4">How do I install software on an RMIT computer?</a></li>
                            <li class="list-group-item"><a class="list-faq" href="{{ url('easter') }}">The building is on fire..!</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                        <img class="img-responsive" src="img/dog.png" alt="">
                    </div>
                </div>
            @include('components.faq-1')
            @include('components.faq-2')
            @include('components.faq-3')
            @include('components.faq-4')
            </div>
            <!-- /.container -->

        </div>
        <!-- /.content-section-b -->

        <div class="content-section-a">

            <div class="container">

                <div class="row">
                    <div class="col-lg-5 col-sm-6">
                        <hr class="section-heading-spacer">
                        <div class="clearfix"></div>
                        <h2 class="section-heading">Looking for something else?</h2>
                        <p class="lead">To learn more about the WDA platform, and the parent organisation please visit the <a target="_blank" href="https://www.rmit.edu.au/">RMIT</a> website. </p>
                        <p class="lead">Alternatively seek help at the original RMIT ITS <a target="_blank" href="http://rmit.service-now.com/serviceandsupport/">service and support page.</a></p>
                    </div>
                    <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                        <img class="img-responsive" src="img/phones.png" alt="">
                    </div>
                </div>

            </div>
            <!-- /.container -->

        </div>
        <!-- /.content-section-a -->

    @endsection