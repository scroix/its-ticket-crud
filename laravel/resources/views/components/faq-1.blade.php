<!-- Modal -->
<div class="modal fade " id="faq-1" tabindex="-1" role="dialog" aria-labelledby="responsesLabel" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center modal-xs">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                    </button>
                    <h5 class="modal-title" id="responsesLabel">How do I log into my RMIT account?</h5>
                </div>
                    <div class="modal-body">
                        <p>1. Navigate to the top of the page.</p>
                        <p>2. Select the login button.</p>
                        <img class="img-responsive" src="img/faq-1.png" alt=""><br>
                        <p>3. Enter your credentials.</p>
                        <p>4. Success!</p>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
