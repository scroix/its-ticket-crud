<!-- Modal -->
<div class="modal fade " id="faq-3" tabindex="-1" role="dialog" aria-labelledby="responsesLabel" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center modal-xs">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                    </button>
                    <h5 class="modal-title" id="responsesLabel">How do I change or reset my password?</h5>
                </div>
                <div class="modal-body">
                    <p>In order to change or reset your password, simply follow these simple instructions.</p><br>
                    <img class="img-responsive" src="img/faq-3.gif" alt=""></p><br>
                    <p>Success!</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
