<!-- Modal -->
<div class="modal fade " id="responses" tabindex="-1" role="dialog" aria-labelledby="responsesLabel" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center modal-md">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                    </button>
                    <h4 class="modal-title" id="responsesLabel">Add response to ticket</h4>

                </div>
                <div class="modal-body">
                    @isset ($ticket)
                    {!! Form::model($ticket, ['action' => 'ResponseController@store']) !!}
                    <div class="form-group">
                        {!! Form::text('ticket_id', '', ['class' => 'form-control ticket-id-input']) !!}

                        {!! Form::label('status', 'Status ') !!}
                        <br>
                        {!! Form::select('status', ['Pending' => 'Pending', 'In Progress' => 'In Progress', 'Unresolved' => 'Unresolved', 'Resolved' => 'Resolved'], null, ['placeholder' => ' ', 'class' => 'form-control']) !!}
                        <br>
                        {!! Form::label('comment', 'Comment') !!}
                        {!! Form::textarea('response', '', ['class' => 'form-control', 'size' => '9x4']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary">
                </div>
                {!! Form::close() !!}
                @endisset
            </div>
        </div>
    </div>
</div>
