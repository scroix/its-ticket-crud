<!-- Modal -->
<div class="modal fade " id="faq-2" tabindex="-1" role="dialog" aria-labelledby="responsesLabel" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center modal-xs">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                    </button>
                    <h5 class="modal-title" id="responsesLabel">I can't login</h5>
                </div>
                <div class="modal-body">
                    <p>If you're having trouble logging into RMIT services, don't be alarmed.</p>
                    <p><i>You're not alone.</i></p>
                    <p>If you are in an emergency, or risk of harm to yourself or others, please contact...</p>
                    <ul>
                        <li>RMIT Call Back Service - 1300 248 498</li>
                        <li>Student Support Network - 9582 0941</li>
                        <li>Helpline - 14 94 18</li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
