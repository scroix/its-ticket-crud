<html>
<head>
    <title> @yield('title') </title>

    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="{{ URL::asset('js/chic.js') }}"></script>
</head>
@section('title', 'Home')
    <body>
    <h1 style="text-align : center; margin-top: 25%; font-size: 150px;">RUN!</h1>
    <script type="text/javascript">
        function Cycle(array) {
            var i = 0;
            this.next = function () {
                i %= array.length;
                return array[i++];
            }
        }

        var colours = new Cycle(['rgb(255,0,0)','rgb(255,255,255)']);
        var inverted_colours = new Cycle(['rgb(255,255,255)','rgb(255,0,0)']);

        $('body').css('background-color', colours.next());
        $('h1').css('color', inverted_colours.next());

        setInterval(function () {
            $('h1').css('color', inverted_colours.next());
            $('body').css('background-color', colours.next());
        }, 500);

    </script>
    </body>
    </html>