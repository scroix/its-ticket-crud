@extends('layouts.master', ['template' => 'user'])
@section('title', 'Submission')
@section('content')
    <div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Ticket</h2>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    {!! Form::model($ticket, ['action' => 'TicketController@store']) !!}
    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', Auth::user()->name, ['class' => 'form-control', 'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('email', 'E-Mail') !!}
        {!! Form::text('email', Auth::user()->email, ['class' => 'form-control', 'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('type', 'Type') !!} <br>
        {!! Form::select('type', ['Request' => 'Request', 'Incident' => 'Incident'], 'Request', ['placeholder' => '', 'class' => 'form-control', 'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('os', 'OS') !!}
        {!! Form::select('os', ['macOS' => 'macOS', 'Windows' => 'Windows', 'Linux' => 'Linux'], null, ['placeholder' => '', 'class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('software', 'Software') !!}
        {!! Form::text('software', '', ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('comment', 'Comment') !!}
        {!! Form::textarea('comment', '', ['class' => 'form-control', 'size' => '9x2']) !!}
    </div>

    <button class="btn btn-success" type="submit">Add the Ticket!</button>

    </div>


    {!! Form::close() !!}
@endsection