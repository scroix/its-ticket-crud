@extends('layouts.master', ['template' => 'user'])
@section('title', 'Track')
@section('content')
    <div class="container">
        <br><br>
        @if ($patron = Auth::user())
        <h2>{{$patron->email}}</h2>
        <h4>{{$patron->name}}</h4>
        @else
        <h2>No Active User</h2>
        <h4>Please log in to view tickets.</h4>
        @endif
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">Tickets</div>
            <!-- Table -->
            <table class="table">
                <tr class="highlight">
                    <th>Created</th>
                    <th>Status</th>
                    <th>Platform</th>
                    <th>Software</th>
                    <th>Comment</th>
                    <td></td>
                </tr>
                @if ($patron = Auth::user())
                @foreach ($tickets as $ticket)
                    @if ($ticket->user->email === $patron->email)
                    <tr data-toggle="collapse" data-target=".hidden-responses-{{ $ticket->id }}" class="clickable highlight" value="unselected">
                        <td> {{ $ticket->created_at }}</td>
                        <td> {{ $ticket->status }}</td>
                        <td> {{ $ticket->os }}</td>
                        <td> {{ $ticket->software }}</td>
                        <td colspan="3"> {{ $ticket->comment }}</td>
                    </tr>
                    @endif
                    <tr id="{{ $ticket->id }}" class="collapse response hidden-responses-{{ $ticket->id }}" style="background-color: lightyellow";>
                        <td></td>
                        <td colspan="1"><i>Comment</i></td>
                        <td colspan="5"> {{ $ticket->comment }} </td>
                    </tr>
                    @foreach ($ticket->responses as $response)
                        @if ($loop->remaining < 5)
                            <tr id="{{ $ticket->id }}" class="collapse response hidden-responses-{{ $ticket->id }}" style="background-color: lightyellow";>
                                <td> {{ $response->created_at }}</td>
                                <td> <i>Response</i> </td>
                                <td colspan="5"> {!! strip_tags($response->response, '<strong><em><code><underline><ins>') !!}</td>
                            </tr>
                        @endif
                    @endforeach
                @endforeach
                @endif
            </table>
        </div>
        @if ($patron = Session::get('patron'))
        @include('components.modal')
        @endif
    </div>
@endsection