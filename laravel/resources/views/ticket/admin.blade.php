@extends('layouts.master', ['template' => 'admin'])
@section('title', 'Admin')
@section('content')
<div class="container">
    <h2> Tickets </h2>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">All</div>
        <!-- Table -->
        <table class="table tickets">
            <tr class="highlight">
                <th class="row-created">Created</th>
                <th class="row-patron">Patron</th>
                <th class="row-status">Status</th>
                <th class="row-type">Type</th>
                <th class="row-platform">Platform</th>
                <th class="row-software">Software</th>
                <th class="row-comment">Comment</th>
                <th class="row-delete"></th>
            </tr>
            @foreach ($tickets as $ticket)
            <tr data-toggle="collapse" data-target=".hidden-responses-{{ $ticket->id }}" class="clickable highlight" value="unselected">
                <td> {{ $ticket->created_at }}</td>
                <td> {{ $ticket->patron->email }}</td>
                <td class="status"> {{ $ticket->status }}</td>
                <td> {{ $ticket->type }}</td>
                <td> {{ $ticket->os }}</td>
                <td> {{ $ticket->software }}</td>
                <td> {{ $ticket->comment }}</td>
                <td class="btn-delete">
                {!! Form::open(['method' => 'DELETE','route' => ['admin.destroy', $ticket],'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['span class' => 'btn-xs btn-danger']) !!}
                {!! Form::close() !!}
                </td>
                <!-- <td style="vertical-align: middle"><span class="glyphicon glyphicon-remove"></span></td> -->
            </tr>
            <tr id="{{ $ticket->id }}" class="collapse response hidden-responses-{{ $ticket->id }}" style="background-color: lightyellow";>
                <td></td>
                <td colspan="1"><i>Comment</i></td>
                <td colspan="6"> {{ $ticket->comment }} </td>
            </tr>
                @foreach ($ticket->responses as $response)
                    @if ($loop->remaining < 5)
                    <tr id="{{ $ticket->id }}" class="collapse response hidden-responses-{{ $ticket->id }}" style="background-color: lightyellow";>
                        <td> {{ $response->created_at }}</td>
                        <td colspan="1"><i>Response</i></td>
                        <td colspan="6"> {{ $response->response }}</td>
                    </tr>
                    @endif
                @endforeach
            <tr id="{{ $ticket->id }}" class="collapse hidden-responses-{{ $ticket->id }}" style="background-color: lightcoral; color: white">
                <td colspan="8" class="toggle_response" value="{{ $ticket->id }}" data-toggle="modal" data-target="#responses">
                    <span class="glyphicon glyphicon-plus"></span> Add response to ticket
                </td>
            </tr>
            @endforeach
        </table>
    </div>
    {!! $tickets->render() !!}
    @include('components.modal')
</div>
@endsection