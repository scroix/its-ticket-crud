<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-inline">
                    <li>
                        <a href="#">Home</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#about">Disclaimer</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#services">Privacy</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#contact">Feedback</a>
                    </li>
                </ul>
                <p class="copyright text-muted small">Copyright &copy; RMIT Web Database Applications - 2017. All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>