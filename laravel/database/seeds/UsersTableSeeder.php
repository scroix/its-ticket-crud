<?php

use Illuminate\Database\Seeder;

use a2\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Clear existing table.
        // User::getQuery()->delete();

        $faker = Faker\Factory::create();

        $limit = 5;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('users')->insert([ //,
                'email' => $faker->unique()->email,
                'name' => $faker->name,
                'password' => $faker->password,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }
}
