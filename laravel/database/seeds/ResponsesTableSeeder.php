<?php

use Illuminate\Database\Seeder;

use a2\Models\Ticket;
use a2\Models\TIcketResponse;

class ResponsesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Clear existing table.
        TicketResponse::getQuery()->delete();

        $faker = Faker\Factory::create();

        $limit = 20;

        $ticket = Ticket::all()->pluck('id')->toArray();

        for ($i = 0; $i < $limit; $i++) {
            DB::table('ticket_responses')->insert([ //,
                'response' => $faker->realText($maxNbChars = 50, $indexSize = 1),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'ticket_id' => $faker->randomElement($ticket),
            ]);
        }
    }
}
