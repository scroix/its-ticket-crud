<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Ticket;

class DatabaseSeeder extends Seeder
{
    // php artisan db:seed
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // php artisan db:seed
        $this->call(UsersTableSeeder::class);
        $this->call(TicketsTableSeeder::class);
        $this->call(ResponsesTableSeeder::class);
    }
}
