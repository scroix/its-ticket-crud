<?php

use Illuminate\Database\Seeder;

use a2\Models\Ticket;
use a2\Models\User;

class TicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Clear existing table.
        Ticket::getQuery()->delete();

        $faker = Faker\Factory::create();

        $limit = 20;

        $user = User::all()->pluck('id')->toArray();
        $os = ['macOS', 'Windows', 'Linux'];
        $status = ['Pending', 'In Progress', 'Unresolved', 'Resolved'];
        $escalation = ['1', '2', '3'];
        $priority = ['Low', 'Moderate', 'High'];

        for ($i = 0; $i < $limit; $i++) {
            DB::table('tickets')->insert([ //,
                'status' => $faker->randomElement($status),
                'escalation' => $faker->randomElement($escalation),
                'priority' =>$faker->randomElement($priority),
                'os' => $faker->randomElement($os),
                'software' => $faker->colorName,
                'comment' => $faker->realText($maxNbChars = 100, $indexSize = 1),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'user_id' => $faker->randomElement($user),
            ]);
        }
    }
}
