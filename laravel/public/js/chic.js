$(document).ready(function() {

    // Manage whether rows have been "selected" or not.
    $("tr.highlight").slice(1).click(function(){
        if ($(this).attr("value") == "unselected") {
            $(this).attr("value", "selected");
        }
        else {
            $(this).attr("value", "unselected");
        }
    });

    // Highlight rows on mouse-over, as long as they haven't been selected.
    $("tr.highlight").slice(1).hover(function(){
        if ($(this).attr("value") == "unselected") {
            $(this).css("background-color", "#F0F0F0");
        }
    }, function(){
        if ($(this).attr("value") == "unselected") {
            $(this).css("background-color", "white");
        }
    });

    // Pass values from rows to modal
    $(".toggle_response").click(function () {
        $(".ticket-id-input").val($(this).attr("value"))
    })

    // Active tooltips
    $('[data-toggle="tooltip"]').tooltip();

});