<?php

namespace a2\Http\Controllers;

use Illuminate\Http\Request;
use a2\Models\Ticket;
use a2\Models\User;
use a2\Models\TicketResponse;

class WaiterController extends Controller
{
    // Serve up all tickets from the respective table, and tack on the associated email
    // through the foreign key relationship.
   public function index()
   {
       $tickets = Ticket::all();

       $responses = array();
       // We're building a custom version of the returned tickets array.
       // Our version collates data from two different tables before being made available.
       foreach ($tickets as $ticket) {
           $response = [
               'id'         => $ticket->id,
               'status'     => $ticket->status,
               'priority'   => $ticket->priority,
               'escalation' => $ticket->escalation,
               'os'         => $ticket->os,
               'software'   => $ticket->software,
               'comment'    => $ticket->comment,
               'user'       => $ticket->user->email, // foreign key relationship
               'created'    => $ticket->created_at,
               'updated'    => $ticket->updated_at,
           ];
            array_push($responses, $response);
       }

       // Content-Type and Accept headers must be set to 'application/json'.
       return $responses;
   }

   // Serve up all the RESPONSES for a given ticket.
   public function show(Ticket $ticket)
   {
       // Grab the responses through association.
       $response = $ticket->responses;

       // Content-Type and Accept headers must be set to 'application/json'.
       return $response;
   }

   // Store a new ticket RESPONSE provided by the user.
   public function store(Request $request)
   {
       /*
       $ticket = Ticket::create($request->all());
       */

       $response = TicketResponse::create($request->all());

       return response()->json($response, 201); // 201: Object created.
   }

   // Update the details of an existing ticket.
   public function update(Request $request, Ticket $ticket)
   {
       $ticket->update($request->all());

       return response()->json($ticket, 200); // 200: Success code.
   }

   // Delete an existing ticket.
   public function delete(Ticket $ticket)
   {
       $ticket->delete();

       return response()->json(null, 204); // 204: Success, but no content to return.
   }
}
