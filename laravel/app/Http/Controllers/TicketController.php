<?php

namespace a2\Http\Controllers;

use a2\Http\Requests\TicketFormRequest;
use Illuminate\Http\Request;
use a2\Models\Ticket;
use a2\Models\User;

class TicketController extends Controller
{
    public function create()
    {
        $ticket = new Ticket;
        return view('ticket.create', ['ticket' => $ticket ]);
    }

    public function index()
    {
        return \View::make('ticket.track')
            ->with('tickets', Ticket::all());
    }

    public function store(TicketFormRequest $request)
    {
        // Gather up all the form data.
        $allRequest = $request->all();

        // Search the database for pre-existing user which matches their e-mail.
        $user = \Auth::user();

        // Create our new ticket from the form data.
        $ticket = new Ticket();
        $ticket->status = 'Pending';
        $ticket->escalation = 1;
        $ticket->priority = 'Low';
        $ticket->os = $allRequest['os'];
        $ticket->software = $allRequest['software'];
        $ticket->comment = $allRequest['comment'];
        $ticket->user_id = $user->id;
        $ticket->save();

        // Store in session.
        //$request->session()->put('user', $user);

        return redirect()->route('ticket.create') -> with('success', 'Ticket submitted successfully');
    }

    public function destroy($id)
    {
        Ticket::find($id)->delete();
        return redirect()->route('ticket.index')
            ->with('success','Your ticket has been deleted successfully');
    }

}
