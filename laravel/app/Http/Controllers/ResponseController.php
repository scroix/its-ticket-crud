<?php

namespace a2\Http\Controllers;

use Illuminate\Http\Request;

use a2\Models\Ticket;
use a2\Models\TicketResponse;

class ResponseController extends Controller
{
    public function index(Request $request)
    {
        $tickets = Ticket::orderBy('id', 'DESC')->paginate(15);
        return view('ticket.admin', compact('tickets')) ->with('i', ($request->input('page', 1) -1) * 15);
    }

    /*
      return \View::make('admin.overview')
         ->with('tickets', Ticket::all());
     */

    public function create()
    {
        $response = new TicketResponse;
        return view('ticket.admin', ['response' => $response ])
            ->with('tickets', Ticket::all());
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'response' => 'required|max:150'
        ]);

        // Gather up all the form data.
        $allRequest = $request->all();

        // Create our new ticket from the form data.
        $response = new TicketResponse();
        $response->ticket_id = $allRequest['ticket_id'];
        $response->response = $allRequest['response'];
        $response->save();

        Ticket::where('id', $response->ticket_id)->update(array('status' => $allRequest['status']));

        return redirect()->route('admin.index')
            -> with('success', 'Ticket updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ticket::find($id)->delete();
        return redirect()->route('admin.index')
            ->with('success','Ticket deleted successfully');
    }

}
