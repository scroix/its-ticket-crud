<?php

namespace a2\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [ 'email', 'status', 'os', 'comment', 'escalation', 'priority' ];

    public function user() {
        return $this->belongsTo('a2\Models\User');
    }

    public function responses() {
        return $this->hasMany('a2\Models\TicketResponse');
    }

    /**
     * We're going to re-format the created_at and modified_at columns which are created using
     * the timestamp() function in our migration files. This will simply the representation in our views
     * but retain the extended data in our database.
     */

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    public function getUpdatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }
}
